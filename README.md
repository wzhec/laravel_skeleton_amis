# Laravel 骨架包

> 自用的api基础库

1. 引入amis低代码框架便于管理
2. api支持

## 使用方法

1. 创建项目
   `composer create-project iwzh/laravel-skeleton -vvv`
2. 创建配置
   `cp .env.example .env`
3. 修改 .env 文件中数据库配置为真实的配置
4. 创建数据表和测试数据
   `php artisan migrate --seed`

## 使用后端管理包
* 目前用了合作开发的的一个包
> laravel 11 需要先执行 php artisan install:api

* 安装 admin 包
  ```composer require slowlyo/owl-admin```
* 执行命令发布资源
  ```php artisan admin:publish```
* 安装命令
  ```php artisan admin:install```

> Tip
>执行这一步命令可能会报以下错误Specified key was too long ... 767 bytes
>如果出现这个报错，请在app/Providers/AppServiceProvider.php文件的boot 方法中加上代码\Schema::defaultStringLength(191);
>然后删除掉数据库中的所有数据表，再重新运行一遍php artisan admin:install命令即可。
