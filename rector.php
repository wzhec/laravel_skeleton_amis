<?php

declare(strict_types=1);
/**
 * This file is part of Laravel-Skeleton.
 *
 * @link     https://www.wangzenghui.com
 * @document https://blog.wangzenghui.com
 * @contact  wzhec@foxmail.com
 * @license  https://gitee.com/wzhec/Laravel-Skeleton/blob/main/LICENSE
 */
use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Config\RectorConfig;
use Rector\PHPUnit\Set\PHPUnitSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        __DIR__ . '/src/*/tests',
    ]);

    // register a single rule
    $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);

    // define sets of rules
    $rectorConfig->sets([
        PHPUnitSetList::PHPUNIT_100,
    ]);
};
